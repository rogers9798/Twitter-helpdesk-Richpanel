# Twitter-helpdesk-Richpanel

Twitter Helpdesk assignment for Richpanel
<br/>POC (Proof of Concept) app. This app will allow clients
to connect their twitter accounts.

## Working : 

1. Client will login to your app
2. They’ll be prompted to login using Twitter. Your app will also ask for permission to fetch tweets and reply to tweets for their Twitter handle.
3. MainPage UI will be shown

### Production app on Heroku : https://twitter-helpdesk-richpanel.herokuapp.com/

## Development : 

#### 1. Clone the Repository

```Bash
git clone https://gitlab.com/rogers9798/Twitter-helpdesk-Richpanel.git
cd Twitter-helpdesk-Richpanel
```

#### 2. Install the dependencies

```BASH
npm install

// for client side :

cd client
npm install
```

#### 3. Run backend server: ( in App root directory )

```BASH
npm server.js
```

#### 4. Run Frontend Server : ( in client directory )

```BASH
cd client
npm start
```

## Twitter API Keys

You will need Twitter API Keys to authorize login using Twitter and PassportJS

* API Key
* API Secret Key

Store the above keys in `.env` file in project's root directory.

## MongoDB URI

MongoDB URI is needed to store tweet data and user data.<br/>
I have used MongoAtlas for the above use case.

## Working Screenshots : 
<br/>

![login](./assets/login.png)<br/>
![auth](./assets/auth.png)<br/>
![login](./assets/main.png)<br/>

<pre> Note : Add these `localhost` callback url in twitter developer portal in your app a/c to the port and routes</pre>
![callback](./assets/callback.png)<br/>

## Working Video : 

**Click on the image below to watch video :**

[![Watch the video](./assets/login.png)](https://drive.google.com/file/d/1NZfHxNxh4X0KreNJNyVSNkwJNV4qT9_a/view?usp=sharing)


### Thanks for the opportunity 
<pre>Sachin Kumar
JIIT-62
sac9798@gmail.com
</pre>
