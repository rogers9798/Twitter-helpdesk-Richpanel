require('dotenv').config();

const mongodb = {
  mongo_uri: process.env.mongo_uri,
};

const session = {
  cookie_key: 'helpdesk',
};

const twitter_key = {
  api: process.env.API_KEY,
  api_secret: process.env.API_SECRET_KEY,
};

const keys = {
  ...mongodb,
  ...session,
  ...twitter_key,
};

module.exports = keys;
