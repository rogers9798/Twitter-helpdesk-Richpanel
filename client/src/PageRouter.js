import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import Home from './components/Home/Home.jsx';

class PageRouter extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path='/'>
            <Home />
          </Route>
        </div>
      </Router>
    );
  }
}

export default PageRouter;
