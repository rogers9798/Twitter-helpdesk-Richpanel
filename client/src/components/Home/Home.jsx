import React, { Component } from 'react';
import Navbar from '../Navbar/navbar.jsx';
import Login from '../Login/Login.jsx';
import PropTypes from 'prop-types';
import Sidebar from '../MainComponent/sidebar.jsx';
import Header from '../MainComponent/Header.jsx';
import SubHeader from '../MainComponent/SubHeader.jsx';
// import Profile from '../MainComponent/Profile.jsx'
import Conversations from '../MainComponent/Conversation.jsx';

class Home extends Component {
  static propTypes = {
    user: PropTypes.shape({
      Name: PropTypes.string,
      userName: PropTypes.string,
      TwitterID: PropTypes.string,
      UserImg: PropTypes.string,
      _id: PropTypes.string,
    }),
  };

  state = {
    user: {},
    error: null,
    authenticated: false,
  };

  componentDidMount() {
    fetch(
      'https://twitter-helpdesk-richpanel.herokuapp.com/auth/login/success',
      {
        method: 'GET',
        credentials: 'include',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Credentials': true,
        },
      }
    )
      .then((response) => {
        if (response.status === 200) return response.json();
        throw new Error('User Authentication failed');
      })
      .then((responseJson) => {
        console.log('response to req  ', responseJson);
        this.setState({
          authenticated: true,
          user: responseJson.user,
        });
      })
      .catch((error) => {
        console.log('error is :' + error);
        this.setState({
          authenticated: false,
          error: 'User Authentication failed',
        });
      });
  }

  render() {
    const { authenticated } = this.state;
    return (
      <div>
        {!authenticated ? (
          <div>
            <Navbar
              authenticated={authenticated}
              handleNotAuthenticated={this._handleNotAuthenticated}
            />
            <Login />
          </div>
        ) : (
          <div>
            <Sidebar
              user={this.state.user}
              authenticated={authenticated}
              handleNotAuthenticated={this._handleNotAuthenticated}
            />
            <Header user={this.state.user} />
            <SubHeader user={this.state.user} />
            <Conversations user={this.state.user} />
            {/* <Profile/> */}
          </div>
        )}
      </div>
    );
  }

  _handleNotAuthenticated = () => {
    this.setState({ authenticated: false });
  };
}

export default Home;
