import React from 'react';
import {
  Button,
  CssBaseline,
  Typography,
  makeStyles,
  Container,
} from '@material-ui/core';
import TwitterIcon from '@material-ui/icons/Twitter';

const useStyles = makeStyles((theme) => ({
  page: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  t_icon: {
    margin: theme.spacing(1),
  },
}));

const SignIn = () => {
  const classes = useStyles();

  const handleSignIn = async (e) => {
    e.preventDefault();
    window.open(
      'https://twitter-helpdesk-richpanel.herokuapp.com/auth/twitter',
      '_self'
    );
  };

  return (
    <Container
      component='div'
      maxWidth='xs'
    >
      <CssBaseline />
      <div className={classes.page}>
        <Typography component='h1' variant='h5'>
          Twitter HelpDesk
        </Typography>
        <Button
          type='submit'
          variant='contained'
          color='primary'
          className={classes.submit}
          onClick={handleSignIn}
        >
          <TwitterIcon className={classes.t_icon} />
          LogIn With Twitter
        </Button>
        OR SignUP below
        <Button
          type='submit'
          variant='contained'
          color='primary'
          className={classes.submit}
          onClick={handleSignIn}
        >
          <TwitterIcon className={classes.t_icon} />
          SignUP With Twitter
        </Button>
      </div>
    </Container>
  );
};

export default SignIn;
