import styled from 'styled-components';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import ParentTweet from './Tweets/ParentTweets.jsx';
import ChildTweet from './Tweets/ChildTweets.jsx';
import Profile from './Profile.jsx'

const Converse = styled.div`
  display: flex;
  flex-direction: row;
  padding-left: 70px;
`;

const Converseleft = styled.div`
  width: 95%;
  padding: 10px 80px;
`;

const Tweets = styled.div`
  padding-top: 20px;
  width: 100%;
  display: flex;
  flex-direction: row;
`;

const TweetsLeft = styled.div`
  width: 21%;
  overflow-y: scroll;
  height: 81vh;
  &::-webkit-scrollbar {
    display: none;
  }
`;

const TweetsExpired = styled.div`
  width: 100%;
  text-align: center;
  border-bottom: 1px solid #e6e6e6;
  line-height: 0.1em;
  margin: 20px 0 20px;
`;

const CSpan = styled.span`
  background: #fff;
  padding: 0 10px;
`;

const TweetsRight = styled.div`
  width: 79%;
  margin-left: 30px;
  border: 1px solid #dbdbdb;
  border-radius: 5px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

class Conversations extends Component {
  static propTypes = {
    user: PropTypes.string.isRequired,
  };
  render() {
    // const { user } = this.props;
    return (
      <Converse>
        <Converseleft>
          <Tweets>
            <TweetsLeft>
              {/* <ParentTweet /> */}
              <TweetsExpired>
                <CSpan>Expired Chats</CSpan>
              </TweetsExpired>
            </TweetsLeft>
            <TweetsRight>
              <ChildTweet user={this.props.user} />
              <Profile/>
            </TweetsRight>
          </Tweets>
        </Converseleft>
      </Converse>
    );
  }
}

export default Conversations;
