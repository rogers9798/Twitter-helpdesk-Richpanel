import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Headers = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex: 1;
  color: #a5a5a5;
  font-size:14px;
`;

const LeftHeader = styled.div`
  padding-bottom: 5px;
  padding-left: 150px;
`;

const RightHeader = styled.div`
  color: #878787;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const UserHeader = styled.div`
  padding-left: 50px;
  padding-right: 50px;
  font-weight:700;
`;

const SessionHeader = styled.div`
  padding-right: 50px;
`;

class Header extends Component {
  static propTypes = {
    user: PropTypes.string.isRequired,
  };
  render() {
    const { user } = this.props;
    return (
      <Headers>
        <LeftHeader>Updates</LeftHeader>
        <RightHeader>
          <SessionHeader>Session: 34 minutes</SessionHeader>
          <UserHeader>User: {user.Name}</UserHeader>
        </RightHeader>
      </Headers>
    );
  }
}

export default Header;
