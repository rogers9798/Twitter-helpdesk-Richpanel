import styled from 'styled-components';
import React, { Component } from 'react';
import CloseIcon from '@material-ui/icons/Close';
import Tasks from './Tasks.jsx'

const ProfileDetails = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  min-width: 250px;
  width: 30%;
  border-left: 1px solid #dbdbdb;
  position: relative;
`;

const ProfileDetailsImg = styled.img`
  max-width: 80px;
  border-radius: 50%;
`;
const ProfileDetailsName = styled.div`
  padding-top: 10px;
  font-size: 14px;
  color: black;
`;
const ProfileDetailsStatus = styled.div`
  font-size: 13px;
  padding-top: 2px;
  padding-bottom: 10px;
`;
const ProfileDetailsContact = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  min-width: 170px;
  font-size: 14px;
`;
const ProfileDetailsCall = styled.div`
  background: #f7f6f3;
  border-radius: 10px;
  padding: 4px 14px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const ProfileDetailsCallIcon = styled.img`
  width: 10px;
  height: 10px;
  margin-right: 5px;
`;

const ProfileDetailsEmail = styled.div`
  background: #f7f6f3;
  border-radius: 10px;
  padding: 4px 13px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const ProfileDetailsEmailIcon = styled.img`
  width: 11px;
  height: 11px;
  margin-right: 5px;
`;

const ProfileDetailsTable = styled.div`
  padding-top: 30px;
  min-width: 240px;
`;
const ProfileDetailsRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-bottom: 10px;
`;

class Profile extends Component {
  render() {
    return (
      <ProfileDetails>
        <CloseIcon
          style={{ alignSelf: 'flex-end', margin: '10px 20px' }}
        />
        <ProfileDetailsImg style={ {width:'50px'} } src='https://www.searchpng.com/wp-content/uploads/2019/02/Deafult-Profile-Pitcher.png' />
        <ProfileDetailsName>Rogers</ProfileDetailsName>
        <ProfileDetailsStatus style={{ color: 'darkgrey' }}>
          Online
        </ProfileDetailsStatus>
        <ProfileDetailsContact>
          <ProfileDetailsCall>
            <ProfileDetailsCallIcon src='https://cdn1.iconfinder.com/data/icons/modern-universal/32/icon-03-512.png'></ProfileDetailsCallIcon>
            <span>Call</span>
          </ProfileDetailsCall>
          <ProfileDetailsEmail>
            <ProfileDetailsEmailIcon src='https://image.flaticon.com/icons/png/512/8/8807.png' />
            <span>Email</span>
          </ProfileDetailsEmail>
        </ProfileDetailsContact>

        <ProfileDetailsTable>
          <ProfileDetailsRow style={{ color: 'gray' }}>
            <div>Followers</div>
          </ProfileDetailsRow>
          <ProfileDetailsRow style={{ color: 'gray' }}>
            <div>Following</div>
          </ProfileDetailsRow>
          <ProfileDetailsRow>
            <div style={{ color: 'gray' }}>Username</div>
            <div>@undefined</div>
          </ProfileDetailsRow>
        </ProfileDetailsTable>
        <Tasks/>
      </ProfileDetails>
    );
  }
}

export default Profile;
