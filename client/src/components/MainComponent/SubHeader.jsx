import React, { Component } from 'react';
import styled from 'styled-components';
import Chip from '@material-ui/core/Chip';
import SearchIcon from '@material-ui/icons/Search';
import TuneIcon from '@material-ui/icons/Tune';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const Subheaders = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex: 1;
  padding-top: 30px;
`;

const SubheaderLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-left: 150px;
`;

const SubheaderText = styled.div`
  color: #161616;
  font-size: 22px;
  font-weight: bold;
  padding-right: 20px;
`;

const SubheaderSearchbox = styled.div`
  padding-right: 20px;
`;
const SubheaderFilter = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  background: #f7f6f3;
  padding: 4px 15px;
  border-radius: 15px;
`;
const SubheaderFilterText = styled.div`
  font-size: 14px;
  padding-left: 10px;
`;

const SubheaderRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 2px 7px;
  border: px solid #f0efef;
  border-radius: 10px;
  padding-right: 50px;
`;
const Dot = styled.div`
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background: #29cb41;
`;
const SubheaderOnline = styled.div`
  padding: 0 5px;
`;


class SubHeader extends Component {
  
  render() {
    return (
      <Subheaders style={{ fontFamily: 'Poppins' }}>
        <SubheaderLeft>
          <SubheaderText>Conversations</SubheaderText>
          <SubheaderSearchbox>
            <Chip
              icon={<SearchIcon />}
              label='Quick Search'
              variant='outlined'
              style={{ paddingRight: '50px' }}
              size='small'
            />
          </SubheaderSearchbox>
          <SubheaderFilter>
            <TuneIcon style={{ width: '15px', height: '15px' }} />
            <SubheaderFilterText>Filter</SubheaderFilterText>
          </SubheaderFilter>
        </SubheaderLeft>
        <SubheaderRight>
          <Dot />
          <SubheaderOnline>Online</SubheaderOnline>
          <ArrowDropDownIcon />
        </SubheaderRight>
      </Subheaders>
    );
  }
}

export default SubHeader;
