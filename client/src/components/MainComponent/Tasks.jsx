import styled from 'styled-components';
import React, { Component } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

const Task = styled.div`
  box-sizing: border-box;
  position: absolute;
  bottom: 0;
  width: 100%;
  padding: 8px 20px 10px 10px;
  border-top: 1px solid #dbdbdb;
`;
const TaskHead = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  color: grey;
  font-size: 14px;
`;

const DropdownIcon = styled.img`
  width: 12px;
  height: 10px;
`;

const TaskListItem = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  margin: 10px 0;
  font-size: 15px;
`;

const TaskListItemText = styled.div`
  padding-left: 5px;
`;

const Tfooter = styled.div`
  color: grey;
  font-size: 15px;
  padding: 10px 0 7px 0;
  border-bottom: 1px solid #dbdbdb;
  width: fit-content;
`;

class Tasks extends Component {
  render() {
    return (
      <Task>
        <TaskHead>
          <div>Tasks</div>
          <DropdownIcon src='https://image.flaticon.com/icons/png/512/60/60995.png' />
        </TaskHead>
        <TaskListItem>
          <List>
            <ListItem>
              <input type='checkbox' />
              <TaskListItemText>Clean up room</TaskListItemText>
            </ListItem>
            <ListItem>
              <input type='checkbox' />
              <TaskListItemText>
                Change linen and towels when guests are out
              </TaskListItemText>
            </ListItem>
            <ListItem>
              <input type='checkbox' />
              <TaskListItemText>
                Bring complimentary bottle of red wine
              </TaskListItemText>
            </ListItem>
          </List>
            </TaskListItem>
            <Tfooter>
                All Tasks
            </Tfooter>
      </Task>
    );
  }
}

export default Tasks;
