import styled from 'styled-components';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ChildTweetMsg from './ChildTweetsMsg.jsx';

const ChildTweets = styled.div`
  width: 70%;
  position: relative;
`;

const ChildTweetsHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  border-bottom: 1px solid #dbdbdb;
  padding: 10px 20px;
  position: relative;
`;

// const ChildTweetsProfileImg = styled.div`
//   width: 20px;
//   height: auto;
//   border-radius: 50%;
// `;

const ChildTweetsHeaderRoom = styled.div`
  padding-left: 40px;
`;

const ChildtweetsHeaderTime = styled.div`
  padding-left: 60px;
`;

const ChildTweetsHeaderName = styled.div`
  padding-left: 10px;
`;

const ChildTweetsHeaderMake = styled.div`
  position: absolute;
  right: 30px;
  background: #f7f6f3;
  padding: 5px 15px;
  border-radius: 10px;
`;

const ChildTweetsBody = styled.div`
  padding: 10px;
`;
const ChildTweetsBodyHeader = styled.div`
  padding: 15px 40px;
  color: #9d9d9d;
`;

const ChildTweetsBodyAssigned = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 5px 10px;
  margin: 10px 0;
`;
const ChildTweetsBodyAssignedText = styled.div`
  padding-left: 10px;
  color: grey;
`;

const CTSpan = styled.span`
  color: #bd0f0f;
`;

const Service = styled.img`
  max-width: 20px;
  height: auto;
  border-radius: 50%;
`;

const ChildTweetsBodyReply = styled.div`
  padding-left: 10px;
  position: absolute;
  bottom: 0;
  width: 90%;
`;

const CTReply = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  padding-left: 10px;
  width: 100%;
  position: relative;
`;

const ReplyInput = styled.input`
  outline: none;
  color: grey;
  border: 1px solid #d5d5d5;
  border-radius: 5px;
  height: 40px;
  width: 100%;
  padding-left: 20px;
  margin-left: 20px;
  margin-bottom: 10px;
`;

const ProfileImg = styled.img`
  max-width: 20px;
  height: auto;
  border-radius: 50%;
  margin-top: 2px;
`;

const AttachIcon = styled.img`
  width: 15px;
  height: auto;
  position: absolute;
  right: 20px;
  top: calc(50% - 15px);
`;


class ChildTweet extends Component {
  static propTypes = {
    user: PropTypes.string.isRequired,
  };
  render() {
    const { user } = this.props;
    return (
      <ChildTweets>
        <ChildTweetsHeader>
          {/* sender img */}

          <ChildTweetsHeaderName>{/* user name */}</ChildTweetsHeaderName>
          <ChildTweetsHeaderRoom>Room : 101</ChildTweetsHeaderRoom>
          <ChildtweetsHeaderTime>Jan 16 - Jan 18</ChildtweetsHeaderTime>
          <ChildTweetsHeaderMake>Create a Task</ChildTweetsHeaderMake>
        </ChildTweetsHeader>
        <ChildTweetsBody>
          <ChildTweetsBodyHeader>{/* {getDay()} */}</ChildTweetsBodyHeader>

          <ChildTweetMsg />

          <ChildTweetsBodyAssigned>
            <Service
              style={{ width: '30px' }}
              src='https://banner2.cleanpng.com/20180703/xaw/kisspng-call-centre-computer-icons-customer-service-callce-5b3b3227d72611.9834183315306061198813.jpg'
            ></Service>
            <ChildTweetsBodyAssignedText>
              <CTSpan>{user.Name}</CTSpan> (you) assigned to this conversation
            </ChildTweetsBodyAssignedText>
          </ChildTweetsBodyAssigned>

          {/* <ChildTweetMsg /> */}

        </ChildTweetsBody>
        <ChildTweetsBodyReply>
          <CTReply>
            <ProfileImg
              src={user.UserImg}
              style={{ width: '30px', borderRadius: '50%', marginTop: '2px' }}
            ></ProfileImg>
            <ReplyInput
              type='text'
              placeholder='Reply...'
              onkeydown='reply'
            ></ReplyInput>
            <AttachIcon src='https://cdn3.iconfinder.com/data/icons/miniglyphs/500/064-512.png' />
          </CTReply>
        </ChildTweetsBodyReply>
      </ChildTweets>
    );
  }
}

export default ChildTweet;
