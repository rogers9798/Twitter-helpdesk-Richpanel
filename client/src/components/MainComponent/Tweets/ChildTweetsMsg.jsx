import styled from 'styled-components';
import React, { Component } from 'react';
// import PropTypes from 'prop-types';

const ChildTweetsMsg = styled.div`
  border-radius: 4px;
  display: flex;
  flex-direction: row;
  padding: 5px 10px;
  margin: 20px 0;
`;

const ChildTweetsMsgHeader = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

// const ProfileMsgImg = styled.div`
//   max-width: 20px;
//   height: auto;
//   border-radius: 50%;
//   margin-top: 2px;
// `;

const ChildTweetsMsgRight = styled.div`
  text-align: left;
  padding-left: 10px;
  width: 100%;
`;

const ChildTweetsMsgGreet = styled.div`
  padding-right: 5px;
  font-size: 16px;
  color: black;
`;

const ChildTweetsMsgTime = styled.div`
  position: absolute;
  right: 0;
  font-size: 14px;
  color: grey;
  padding-right: 15px;
`;

// const ChildTweetsMsgDesc = styled.div`
//   font-size: 15px;
//   padding-top: 10px;
// `;

// const ChildTweetsMsgImg = styled.div`
// width: 100px;
//   height: 100px;
//   margin-top: 20px;
//   margin-right: 10px;
//   border-radius: 10px;
// `;

class ChildTweetMsg extends Component {
  render() {
    return (
      <ChildTweetsMsg>
        {/* <ProfileMsgImg /> profile img of sender */}
        <ChildTweetsMsgRight>
          <ChildTweetsMsgHeader>
            <ChildTweetsMsgGreet>{/* {{text}}  */}</ChildTweetsMsgGreet>
            <ChildTweetsMsgTime>{/* {{time}} */}</ChildTweetsMsgTime>
          </ChildTweetsMsgHeader>
        </ChildTweetsMsgRight>
      </ChildTweetsMsg>
    );
  }
}

export default ChildTweetMsg;
