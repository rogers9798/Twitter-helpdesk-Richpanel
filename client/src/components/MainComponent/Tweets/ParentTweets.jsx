import styled from 'styled-components';
import React, { Component } from 'react';
// import PropTypes from 'prop-types';

const Tweetbox = styled.div`
  border: 1px solid lightgrey;
  border-radius: 3px;
  max-height: 60px;
  display: flex;
  flex-direction: row;
  padding: 15px 10px;
  margin-bottom: 10px;
  &:hover {
    cursor: pointer;
  }
`;

const TweetboxLeft = styled.div`
  width: 15%;
`;

const TweetboxProfileImg = styled.div`
  max-width: 20px;
  height: auto;
  border-radius: 50%;
  margin-top: 4px;
`;

const TweetboxRight = styled.div`
  text-align: left;
  padding-left: 15px;
  width: 100%;
`;

const TweetboxHeader = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-bottom: 5px;
  width: 100%;
`;

const TweetboxHeaderName = styled.div`
  padding-right: 5px;
  font-size: 14px;
  color: black;
`;

// const GreenDot = styled.div`
//   width: 8px;
//   height: 8px;
//   background: #5ebb4d;
//   border-radius: 50%;
// `;

const TweetboxHeaderMsgCount = styled.div`
  position: absolute;
  right: 0;
  background: red;
  color: white;
  padding: 2px 4px;
  border-radius: 2px;
  font-size: 11px;
`;

const TweetboxMsg = styled.div`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
  white-space: normal;
  font-size: 13px;
  color: grey;
`;

class ParentTweet extends Component {
  render() {
    return (
      <Tweetbox>
        <TweetboxLeft>
          {/* tweet sender img */}
          <TweetboxProfileImg />
        </TweetboxLeft>

        <TweetboxRight>
          <TweetboxHeader>
            <TweetboxHeaderName>{/* tweet sender name */}</TweetboxHeaderName>
            <TweetboxHeaderMsgCount>
              {/* tweets count */}
            </TweetboxHeaderMsgCount>
          </TweetboxHeader>
          <TweetboxMsg>
            {/* mentioned msg */}
          </TweetboxMsg>
        </TweetboxRight>
      </Tweetbox>
    );
  }
}

export default ParentTweet;
