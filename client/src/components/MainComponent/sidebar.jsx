import React, { Component } from 'react';
import styled from 'styled-components';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import List from '@material-ui/core/List';
import TimerIcon from '@material-ui/icons/Timer';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import ForumOutlinedIcon from '@material-ui/icons/ForumOutlined';
import CreditCardOutlinedIcon from '@material-ui/icons/CreditCardOutlined';
import StorefrontOutlinedIcon from '@material-ui/icons/StorefrontOutlined';
import AdjustIcon from '@material-ui/icons/Adjust';
import PropTypes from 'prop-types';

const SideBarWindow = styled.div`
  height: 100%;
  width: 60px;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #f7f6f3;
  overflow-x: hidden;
  // padding-top: 0px;
`;

const SideBarOptions = styled.div`
  padding-top: 30px;
`;

const Options = styled.div`
  font-weight: 500;
  padding: 0.7rem;
  text-transform: uppercase;
  text-align: center;
  color: grey;
  margin-top: 2px;
`;

class Sidebar extends Component {
  static propTypes = {
    user: PropTypes.string.isRequired,
    // authenticated: PropTypes.bool.isRequired,
  };
  render() {
    const { user } = this.props;
    // const { authenticated } = this.props;
    // const { handleNotAuthenticated } = this.props;
    return (
      <SideBarWindow className='sidebar'>
        <List style={{ textAlign: 'center' }}>
          <FiberManualRecordIcon
            style={{ fontSize: 'small', color: '#ff6058', paddingRight: '5px' }}
          />
          <FiberManualRecordIcon
            style={{ fontSize: 'small', color: '#ffbd2e', paddingRight: '5px' }}
          />
          <FiberManualRecordIcon
            style={{ fontSize: 'small', color: '#29cb41' }}
          />
        </List>
        <svg
          width='36'
          height='34'
          class='pentagon'
          style={{ fill: '#BF392C', paddingLeft: '11px', paddingTop: '10px' }}
        >
          <polygon points='9,0 27,0 36,17 18,34 0,17' />
        </svg>
        <SideBarOptions>
          <Options>
            <TimerIcon />
          </Options>
          <Options>
            <HomeOutlinedIcon />
          </Options>
          <Options>
            <PeopleAltOutlinedIcon />
          </Options>
          <Options style={{ background: '#e8e7e4' }}>
            <ForumOutlinedIcon style={{ color: 'black' }} />
          </Options>
          <Options>
            <CreditCardOutlinedIcon />
          </Options>
          <Options>
            <StorefrontOutlinedIcon />
          </Options>
          <Options>
            <AdjustIcon
              style={{ paddingTop: '95px', cursor: 'pointer' }}
              onClick={this.handleLogoutClick}
            />
          </Options>
          <Options>
            <img
              src={user.UserImg}
              alt='icon'
              style={{ width: '30px', height: '30px', borderRadius: '50%' }}
            ></img>
          </Options>
        </SideBarOptions>
      </SideBarWindow>
    );
  }

  handleLogoutClick = () => {
    window.open(
      'https://twitter-helpdesk-richpanel.herokuapp.com/auth/logout',
      '_self'
    );
    this.props.handleNotAuthenticated();
  };
}

export default Sidebar;
