import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import TwitterIcon from '@material-ui/icons/Twitter';

const useStyles = (theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
});

class Navbar extends Component {
  static propTypes = {
    authenticated: PropTypes.bool.isRequired,
  };

  render() {
    const { authenticated } = this.props;
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position='static'>
          <Toolbar>
            <IconButton
              edge='start'
              className={classes.menuButton}
              color='inherit'
              aria-label='menu'
            >
              <TwitterIcon />
            </IconButton>
            <Typography variant='h6' className={classes.title}>
              <Link to='/' color='inherit'>
                TWITTER HELPDESK
              </Link>
            </Typography>
            {authenticated ? (
              <Button color='inherit' onClick={this._handleLogoutClick}>
                Logout
              </Button>
            ) : (
              <Typography variant='h6' >
                  Please Login!
              </Typography>
            )}
          </Toolbar>
        </AppBar>
      </div>
    );
  }

  _handleLogoutClick = () => {
    window.open(
      'https://twitter-helpdesk-richpanel.herokuapp.com/auth/logout',
      '_self'
    );
    this.props.handleNotAuthenticated();
  };
}

export default withStyles(useStyles)(Navbar);
