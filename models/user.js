const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clientSchema = new Schema({
  Name: String,
  userName: String,
  TwitterID: String,
  UserImg: String,
});

const Client = mongoose.model('client', clientSchema);

module.exports = Client;
