const passport = require('passport');
const express = require('express');

const appRoute = express.Router();

// auth with twitter
appRoute.get('/twitter', passport.authenticate('twitter'));

// redirect to home page after successfully login via twitter
appRoute.get(
  '/twitter/callback',
  passport.authenticate('twitter', {
    successRedirect: 'https://twitter-helpdesk-richpanel.herokuapp.com',
    failureRedirect: '/auth/login/failed',
  })
);

// when login is successful, retrieve user info
appRoute.get('/login/success', (req, res) => {
  if (req.user) {
    res.json({
      success: true,
      message: 'Client Authentication Successful ',
      user: req.user,
      cookies: req.cookies,
    });
  }
});

// when login failed, send failed msg
appRoute.get('/login/failed', (req, res) => {
  res.status(401).json({
    success: false,
    message: 'user failed to authenticate.',
  });
});

// When logout, redirect to client
appRoute.get('/logout', (req, res) => {
  req.logout();
  res.redirect('https://twitter-helpdesk-richpanel.herokuapp.com');
});

module.exports = appRoute;
