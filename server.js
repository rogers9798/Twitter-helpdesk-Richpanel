const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const path = require('path');

const passport = require('passport');
const TwitterStrategy = require('passport-twitter');

const keys = require('./auth/process-keys');
const appRoute = require('./routes/authenticationRoutes');
const Client = require('./models/user');

const cookieSession = require('cookie-session'); //maintaing session using cookies
const cookieParser = require('cookie-parser'); //to parse headers from cookies

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(
  cors({
    origin: 'https://twitter-helpdesk-richpanel.herokuapp.com', // allow to server to accept request from different origin
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
  })
);

app.use(express.static(path.join(__dirname, 'build')));
app.use(express.json());
app.use(
  cookieSession({
    name: 'helpdesk-session',
    keys: [keys.cookie_key],
    maxAge: 24 * 60 * 60 * 100, // time to expire the cookie
  })
);
app.use(cookieParser());
// initalize passport
app.use(passport.initialize());
// deserialize cookie from the browser
app.use(passport.session());

const uri = keys.mongo_uri;
mongoose.connect(uri, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});

const connection = mongoose.connection;
connection.once('open', () => {
  console.log('MongoDB database connection established successfully');
});

passport.serializeUser((user, done) => {
  done(null, user);
});

// deserialize the cookieUserId to user in the database
passport.deserializeUser((id, done) => {
  Client.findById(id)
    .then((user) => {
      done(null, user);
    })
    .catch((e) => {
      done(new Error('Client deserialization failed'));
    });
});

// authentication for Twitter using Passport twitter-strategy
passport.use(
  new TwitterStrategy(
    {
      consumerKey: keys.api,
      consumerSecret: keys.api_secret,
      callbackURL:
        'https://twitter-helpdesk-richpanel.herokuapp.com/auth/twitter/callback',
    },
    async (token, tokenSecret, profile, done) => {
      const currUser = await Client.findOne({
        TwitterID: profile._json.id_str,
      });

      if (!currUser) {
        const newUser = await new Client({
          Name: profile._json.name,
          userName: profile._json.screen_name,
          TwitterID: profile._json.id_str,
          UserImg: profile._json.profile_image_url,
        }).save();

        if (newUser) {
          done(null, newUser);
        }
      } else {
        done(null, currUser);
      }
    }
  )
);

app.use('/auth', appRoute);
app.use('/*', appRoute);

const check = (req, res, next) => {
  if (!req.user) {
    res.status(401).json({
      authenticated: false,
      message: 'Client Authentication not done',
    });
  } else {
    next();
  }
};

app.get('/*', check, (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
  res.status(200).json({
    authenticated: true,
    message: 'Client Authentication successful',
    user: req.user,
    cookies: req.cookies,
  });
});

app.get('/', check, (req, res) => {
  res.status(200).json({
    authenticated: true,
    message: 'Client Authentication successful',
    user: req.user,
    cookies: req.cookies,
  });
});

app.listen(port, () => {
  console.log(`Server running on port : ${port}`);
});
